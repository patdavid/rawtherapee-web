---
title: "Downtime - Moving Servers"
date: 2013-11-19T16:23:00+00:00
author: DrSlony
Tags:
- Website
---



We will shortly be moving to a new server which may result in temporary
downtime. If the website and forum do not work, simply come back a few
hours later, by when the migration should be completed.

