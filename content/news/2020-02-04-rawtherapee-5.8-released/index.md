---
title: "RawTherapee 5.8 Released"
date: 2020-02-04T08:00:00+01:00
author: DrSlony
draft: false
---

RawTherapee 5.8 has been released! Head over to the [Downloads](/downloads/5.8/) page to read the release notes and to get it.

Update from 2020-02-06:
We found an issue with the Windows installer which was missing the Profiled Lens Correction database (Lensfun), so there is now a new installer from 2020-02-06 which includes the database. If you downloaded RawTherapee 5.8 for Windows previously, please download the new build and re-install.

sha256sum:

- `c212b15f39d02bd8f8a54358045116cbac757ea155e5378e09d727a513895928  RawTherapee_5.8.AppImage`
- `2d6f127598ef9a3d36a86657ac53c81b1957b6d9ae97fef1fadd8757f82aa915  RawTherapee_5.8.exe (2020-02-06)`

