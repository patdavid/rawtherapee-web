---
title: "Towards RawTherapee v2.4 - The First Milestone"
date: 2008-04-23T12:05:00+00:00
author: dualon
Tags:
- RT 2.4
---



First of all thank you for the feedback and bug reports, RT is improved
with every little piece of information. The RT DevTeam would like to
encourage everyone: help us this way in the future, too. Our forum is
open to all of you\!  
  
The development of RawTherapee v2.4 has reached the first milestone. The
features below are<span id="lead_end"></span> implemented already:

  - Full EXIF support: The long-waited feature is here, RawTherapee is
    able to import EXIF data from RAWs and export to the output image
    formats, including TIFF. RT supports Makernotes, and standards
    compliance is improved.
  - EXIF Browser: Browse, edit, delete the original EXIF tags. The
    possibility to record additional tags (like author, copyright and
    comment) is also available.
  - IPTC tag support
  - More input ICC profile options
  - A new highlight recovery method ("CIELab Blending"), that promises
    better recovery of color information
  - Histogram now corresponds to cropped area  
  - Runtime theme switching: The look ("skin") of RT can be changed, and
    you don't even need to restart the program. RT will be packed with
    new themes, too.
  - Lots of other minor (and not-so-minor) tweaks: bugfixes, eye-candy,
    some new GUI icons, performance improvements, etc...

Good work needs time, so we don't want to stick to any deadline, but the
next version is released soon. Stay tuned\!

  
dualon

