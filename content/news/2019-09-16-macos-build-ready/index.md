---
title: "5.7 macOS Build Ready"
date: 2019-09-16T16:09:38+02:00
author: DrSlony
draft: false
---

A RawTherapee 5.7 build for macOS is now available for download.

Caveat: In the latest macOS 10.15 Catalina Developer Beta 8, file access is restricted to `~` and `~/Pictures`.

sha256sum:  
`70f7b7fec75f2799d80b2f960cf98815d5a5b199342d993a6019eeb153332c3d  RawTherapee_OSX_10.9_64_5.7.zip`
