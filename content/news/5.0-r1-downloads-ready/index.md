---
title: "5.0-r1 Downloads Ready"
date: 2017-02-05T22:14:00+00:00
author: DrSlony
---



You can now download RawTherapee 5.0-r1 for Windows and macOS from
our [Downloads](downloads "RawTherapee Downloads page") page. Linux
users can get it from the usual place - their package manager;
instructions available on our Downloads page.


Windows users: If you ran into the "There is no disk in the drive"
error, we just updated the installer with a version which uses GTK+ 3.18
which does not exhibit this problem. It's a fast "release" type build
and it also includes a "debug" executable. Get it now from our Downloads
page.

