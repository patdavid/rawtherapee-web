---
title: "New 5.10 macOS Build"
date: 2024-03-06T05:30:00+00:00
author: Lawrence37
draft: false
---

We received reports of the macOS build not starting for some users. The problem
has been identified, and we have a new build available on the [Downloads](/downloads/5.10/) page. If you encountered an error starting the previous macOS build, please try the updated build.

sha256sum:
- `7208691efbaa077d5512cc9a54897a6bb80c999e942b77b8fdf8872f519ed5ea  RawTherapee_macOS_13.3_Universal_5.10_folder/RawTherapee_macOS_13.3_Universal_5.10.dmg`
