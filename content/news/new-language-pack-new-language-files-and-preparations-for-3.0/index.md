---
title: "New Language pack: new language files and preparations for 3.0"
date: 2010-02-27T17:15:00+00:00
author: DrSlony
Tags:
- RT 3.0
---



Please be informed about the new language pack available for RawTherapee
3.0 alpha1 and earlier (This language packs are also usable on other RT
2.4 version and RT 2.3). There are a few strings in it regarding
3.0alpha1 (but not all strings are translatable yet).

The following things changed since the last
language pack:

  - new language: Brasilian Portuguese
  - updated languages: latvian, english, russian
  - added new available strings to all files (not all available yet)

Many thanks to all contributors for their efforts.

  - Please download a Windows installer
    [here](http://www.rawtherapee.com/data/RT30a1-langPack-20100227.exe "Windows Installer")
  - Please download a tgz archive for Linux or Windows
    [here](http://www.rawtherapee.com/data/RT30a1-langPack-20100227.tgz "TGZ  Archive")
  - you can download the new files also from the release/languages
    directory from the
    [repository](http://code.google.com/p/rawtherapee/source/browse/trunk "Google Code Repository")

The last 10 added languages in chronological order are:

  - Brasilian Portuguese 27.02.2010
  - Catalan 18.10.2009
  - Norwegian 12.02.2009
  - British English 19.01.2009
  - Traditional Chinese 18.08.2008
  - Suomi 18.08.2008
  - Finnish 26.06.2008
  - Dansk 24.06.2008
  - Greek 22.05.2008
  - Euskara 03.03.2008
  - Turkish 02.03.2008

Here the complete list of available languages in alphabetical order:

  - Catalan
  - Chinese Simplified & Traditional
  - Czech
  - Dansk (Danish)
  - Deutsch (German)
  - English (US)
  - English (UK)
  - Espanol (Spanish)
  - Euskara (Basque)
  - Francais (French)
  - Greek
  - Hebrew
  - Italian
  - Japanese
  - Latvian
  - Magyar (Hungarian)
  - Nederlands (Dutch)
  - Norwegian (Norsk)
  - Polish
  - Portuguese (Brasilian)
  - Russian
  - Slovak
  - Suomi (Finnish)
  - Swedish
  - Turkish

The RT DevTeam

