---
title: "RawPedia Back Up and Running"
date: 2016-12-29T23:48:00+00:00
author: DrSlony
---



With the fine help of Pat David we have managed to get RawPedia back up
and running\! It is and will remain accessible through the same
sub-domain as always
- [rawpedia.rawtherapee.com](http://rawpedia.rawtherapee.com/)



~~For now the domain will change to [pixls.us](https://pixls.us/) once
you go there (the same domain where we have our
shared [forum](https://discuss.pixls.us/c/software/rawtherapee)), but
in the near future the whole RawPedia will remain available under the
rawtherapee.com domain.~~


Update 2016-12-31: It is done, the whole RawPedia now resolves
to [rawpedia.rawtherapee.com](http://rawpedia.rawtherapee.com/) \!


We would like to thank [Pat David](https://patdavid.net) for making this transition not only
smooth and painless but so in a festive period\! We would also like to
thank those who volunteered to help via GitHub - it means a lot\!

