---
title: "RawTherapee v2.0"
date: 2007-04-04T00:00:00+00:00
author: dualon
---

The long awaited version 2.0 arrived. It is fast, while the image
quality is the same as before. It runs on weaker CPUs and it requires
less memory that version 1.1. The main changes are the following:

- Completely rewritten, blocking-free asynchronous GUI
- Multi-core processor support
- Background saving
- Change history with bookmarks
- Dual view: full image preview + 100% crop view
- Highlight recovery tool
- Chromatic aberration correction tool

Furthermore, we have now a forum and -- most importantly -- a donation page :-)

