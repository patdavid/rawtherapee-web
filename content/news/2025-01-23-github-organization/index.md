---
title: "GitHub Project Moved to Organization"
date: 2025-01-23T14:23:35+01:00
author: DrSlony
draft: true
---

The RawTherapee project has permanently migrated from a personal account to an organization.

The personal account repository was at: https://github.com/Beep6581/RawTherapee/ 

The new organization is at: https://github.com/RawTherapee/RawTherapee/

For you as a user, nothing changes.

As a package maintainer, please update your links accordingly.

As a developer, the old remote should automatically redirect to the new one, but you can also change it manually:
- If you use SSH:
  ```
  git remote set-url origin git@github.com:RawTherapee/RawTherapee.git
  ```
- If you use HTTPS:
  ```
  git remote set-url origin https://github.com/RawTherapee/RawTherapee.git
  ```

Feel free to seek help via the [forum](https://discuss.pixls.us/rawtherapee), and to report issues through [GitHub](https://github.com/Beep6581/RawTherapee).
