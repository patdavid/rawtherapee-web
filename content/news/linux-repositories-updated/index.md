---
title: "Linux repositories updated"
date: 2017-03-03T17:46:00+00:00
author: DrSlony
---



All Linux repositories have been updated and the naming of the
RawTherapee packages has changed. Check the information for your
distribution on
our [Downloads](http://rawtherapee.com/downloads "Link to Downloads section.") page
to make sure you're using the correct package.
