---
title: "New 5.8 Windows Installer, macOS Build Ready"
date: 2020-02-04T08:00:00+01:00
author: DrSlony
draft: false
---

We found an issue with the original 5.8 Windows installer, it was missing the Profiled Lens Correction (Lensfun) database, so there is now a new installer which includes this database. If you downloaded RawTherapee 5.8 for Windows previously, please download the new build and re-install.

The macOS build is now ready.

Head over to the [Downloads](/downloads/5.8/) page to get the builds.

sha256sum:

- `c212b15f39d02bd8f8a54358045116cbac757ea155e5378e09d727a513895928  RawTherapee_5.8.AppImage`
- `2d6f127598ef9a3d36a86657ac53c81b1957b6d9ae97fef1fadd8757f82aa915  RawTherapee_5.8.exe (2020-02-06)`
- `6fe7cd08bd59b1ca9135e587be0d600d62487d0b9baaa6157972135fe3018f8a  RawTherapee_5.8.dmg`
