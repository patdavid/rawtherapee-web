---
title: "We moved our code to GitHub"
date: 2015-08-11T16:31:00+00:00
author: DrSlony
Tags:
- Management
---



As of today, we have stopped using Google Code as our source code host
and issue tracker, and now use GitHub.


<https://github.com/Beep6581/RawTherapee>


Package maintainers please take notice and update your sources.


The [buildRT](http://rawpedia.rawtherapee.com/Linux) script for Linux
will be updated within the next few days.


Issue discussions are being moved right now, and will be done within a
few hours.

