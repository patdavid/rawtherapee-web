---
title: "RawPedia temporarily down"
date: 2016-12-21T00:24:00+00:00
author: DrSlony
---



We are aware that our RawPedia server stopped accepting connections
within the last few hours and are busy working to restore service. The
cause of the problem lies outside our control and may take a few days to
work around. Follow progress in
issue [\#3550](https://github.com/Beep6581/RawTherapee/issues/3550 "GitHub issue #3550: RawPedia is down").
In the meanwhile you are welcome to use our shared forum
at [discuss.pixls.us](https://discuss.pixls.us/c/software/rawtherapee "RawTherapee shared forum")

