---
title: "PLAY RAW 41"
date: 2014-04-16T10:52:00+00:00
author: DrSlony
Tags:
- Community
---



It's been four years and counting that we've been doing a \~monthly
competition where the winner of the previous round gets to send in an
interesting raw file and everyone is invited to stretch their creative
ligaments and do anything possible, as long as it's with RawTherapee, to
develop it. This round's image is from FredDec, and it's a great scene
of the Bardenas Reales Desert in Spain, taken at dawn.


<figure>
<img src="playraw41_freddec_pop3skincustom_700.jpg">
</figure>


This is what FredDec's image looks like with the "Pop 3
Skin" [processing
profile](http://rawpedia.rawtherapee.com/Sidecar_Files_-_Processing_Profiles) applied,
with a small curve boost.


[Take part in PLAY RAW 41
now\!](http://rawtherapee.com/forum/viewtopic.php?p=36828#p36828)

