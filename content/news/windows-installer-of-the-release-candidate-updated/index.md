---
title: "Windows installer of the release candidate updated"
date: 2009-01-29T16:07:00+00:00
author: dualon
Tags:
- Nightly Builds
- RT 2.4
---



I had to withdraw the windows installer since I made a mistake when
making the package. I have updated it, so from now you can download the
correct installer again.

If you were so fast that you downloaded the RC of RawTherapee in the
past few hours please be so kind and do it again.

I apologize,

Gabor

