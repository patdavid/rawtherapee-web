---
title: "Features"
date: 2013-08-08T22:05:00+00:00
author: dualon
Tags:
- Documentation
- Website
- RT 4.0
---



For a complete and up-to-date list of features, see the [RawPedia
Features](http://rawpedia.rawtherapee.com/Features) page.

## High Image Quality

  - **96-bit** (floating point) processing engine.
  - Non-destructive editing.  
  - Get the **most details and least artifacts** from your raw photos
    thanks to modern and traditional demosaicing algorithms: AMaZE, DCB,
    AHD, EAHD, HPHD, IGV, LMMSE, VNG4, Mono and Fast.
  - **Advanced color handling** from white balance to HSV
    (Hue-Saturation-Value) curves, color toning and color management
    including wide-gamut and soft-proofing support.
  - Powerful **CIE Color Appearance Model 2002** (CIECAM02) module.  
  - **Enhanced exposure and tonality tools**: tone and Lab curves,
    highlights and shadows tools, tone mapping (HDR-like effect), etc.
  - Multiple **denoising methods**: luminance, chrominance (both
    rewritten in 2012), impulse (for salt and pepper noise) noise
    reduction.
  - Several tools to **enhance details**: unsharp mask, RL
    deconvolution, contrast by detail levels.

## Efficiency

  - Multi-threaded algorithms for **high performance** (RawTherapee can
    utilize modern processor features, like SSE).
  - **Quick thumbnails** load lightning fast and are replaced later with
    **live thumbnails**.
  - **Batch processing:** convert all the developed images at once
    without loading the processor while you work.
  - **Copy/paste editing parameters** from one file to many other.
    Partially copying and/or pasting is also possible.**  
    **
  - Basic tools immediately at your hands.
  - **Parallel editing** of multiple images in separate editor tabs,
    and/or all at once from the file browser.
  - An optional **secondary display** can be used.

## Versatility

  - **Wide variety of supported cameras**: almost all DSLRs and even
    some medium format bodies are supported.
  - Can load most **raw files** including 16-, 24- and 32-bit raw **HDR
    DNG** images, as well as standard **JPEG**, **PNG** (8- and 16-bit)
    and **TIFF **(8-, 16- and 32-bit logluv) images.
  - Can save JPEG, PNG (8- and 16-bit) and TIFF (8- and 16-bit) images.
  - Advanced control over the algorithms with **many fine-tuning
    parameters and curves**.  
  - Can **send to GIMP or the editing tool of your choice** in one click
    (16-bit TIFF file).  
  - **Command line** usage besides the normal graphical interface.**  
    **
  - **Various layouts**: multiple tabs, single tab with filmstrip,
    vertical tab with filmstrip, dual monitor.

## Freedom for Free

  - RawTherapee is **free and open source** software, meaning you can
    **use it free of charge, wherever you like on whatever hardware you
    like**, as long as you abide by the copyleft GPLv3 license. Download
    the [source code](https://github.com/Beep6581/RawTherapee), modify
    it, feel free to do what comes to mind. We believe in open software.
  - It is **cross-platform** - you can use it on Linux, macOS or
    Windows.
  - **International:** it is available in 25 languages\!

