---
title: "Quick update"
date: 2013-02-26T01:50:00+00:00
author: DrSlony
Tags:
- Documentation
- Versions
---



After many months of hard work, we will be releasing RawTherapee 4.0.10
in a few days - it is faster, more stable, has a bunch of new features,
and is generally better than any other version, so stay tuned\! We have
also updated
the [Documentation](blog/documentation "Documentation section of the blog") and [Screenshots](blog/screenshots "Screenshots section of the blog") pages,
with further updates coming to the other sections soon.

