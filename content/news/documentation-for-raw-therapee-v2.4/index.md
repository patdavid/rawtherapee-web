---
title: "Documentation for Raw Therapee v2.4"
date: 2009-10-27T08:40:00+00:00
author: dualon
Tags:
- Deprecated Content
- Documentation
- Downloads
- RT 2.4
---



If you want to understand how to work with Raw Therapee even better, we
recommend to read the users manual. You can download it in PDF format:

  - [Users Manual for RT v2.4
    (english)](http://www.rawtherapee.com/data/RawTherapeeManual_2.4.pdf "English translation of the Manual"),
    last change: 2009-09-04
  - [Manuel pour RT v2.4
    (french)](http://www.rawtherapee.com/data/RAWTherapeeManuel_2.4_fr.pdf "French translation of the Manual"),
    last change: 2009-09-20, write errors corrected
  - [Benutzerhandbuch für RT v2.4.1
    (german)](http://www.rawtherapee.com/data/RawTherapeeHandbuch_2.4.pdf "German translation of the Manual"),
    last change: 2009-10-21
  - [Manuale dell'utente di RT 2.4
    (italian)](http://www.rawtherapee.com/data/RawTherapeeManual_2.4_it.pdf "Italian translation of the manual"),
    last change 2009-02-08
  - [Japanese Users Manual for RT v.2.4
    (japanese)](http://www.rawtherapee.com/data/RawTherapeeManual_2.4_jp.pdf "Japanese Translation of the Users Manual"),
    new: 2009-07-27
  - [Handleiding RT 2.4
    (netherlands)](http://www.rawtherapee.com/data/RawTherapeeManual_2.4.1_nl.pdf "Italian translation of the manual"),
    last change 2009-09-04, updated for 2.4.1

Here are some additonal<span id="lead_end"></span> informations:

  - [Internal Workflow of RT v2.4
    (english)](http://www.rawtherapee.com/data/RT-Internal-Workflow_2.4.pdf "English translation of the internal Workflow Diagram")
  - [Flux de travail Interne de RT v2.4
    (french)](http://www.rawtherapee.com/data/RT-Flux%20de%20travail%20Interne_2.4_fr.pdf "French translation of the internal Workflow Diagram")
  - [Interner Arbeitsablauf von RT v2.4
    (german)](http://www.rawtherapee.com/data/RT-Interner-Arbeitsablauf_2.4.pdf "German translation of the internal Workflow Diagram"),
    last change: 2009-02-06
  - [Modalità operativa interna di RT 2.4
    (italian)](http://www.rawtherapee.com/data/RT-Internal-Workflow_2.4_it.pdf "Italian translation of the internal Workflow Diagram")

For those who want to translate the english manual into their own
language, here the OpenOffice source of the manual and the internal
workflow (Changes from previous version have been tracked):

  - [Users Manual for RT v2.4 (english -
    OpenOffice)](http://www.rawtherapee.com/data/090904-RawTherapeeManual_2.4.odt),
    last change: 2009-09-04
  - [Internal Workflow for RT v2.4 (english -
    OpenOffice)](http://www.rawtherapee.com/data/081021-RT-Internal-Workflow_2.4.odg),
    last change: 2008-10-21

Please check before you start with a new translation in the
[Localisation
Forum](http://www.rawtherapee.com/forum/viewforum.php?f=4 "Localisation Section of the RawTherapee Forum")
if someone else already started with the same language and post there if
you start to translate something.

If you have translated the manual to a new language, please send the
result to localisation at rawtherapee dot com. This way it can be
provided here for public download.

