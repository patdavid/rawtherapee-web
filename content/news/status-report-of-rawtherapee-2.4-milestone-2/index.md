---
title: "Status report of RawTherapee 2.4 milestone 2"
date: 2008-07-16T19:32:00+00:00
author: dualon
Tags:
- RT 2.4
---



One important goal of the RT DevTeam is to inform you about what's going
on behind the scenes. This time we can't offer any downloads, but some
information about the progression. Please let us emphasize: no downloads
this time, "only" great news\!  
Milestones are "development snapshots", parts of longer and greater
improvements. However, the second milestone
towards RawTherapee 2.4 offers so much that it's development lasts
longer then originally planned.  
  
The brief - and incomprehensive - list of features, changes, fixes:

  - Caching of thumbnails: The file browser is faster than ever, gives
    an excellent user experience.
  - Ranking in file browser: 1 star... 2 star... up to 5, happy time.
    Rank your files and browse them by these ranks.
  - Filter by EXIF: Search and filter your images by their EXIF data.
    Various fields can be set to narrow the list of hits.
  - Move to trash / delete permanently / rename functionality in file
    browser: Basic file operations are requested a lot, so here they
    come\!
  - File monitoring: RawTherapee watches you... if files are
    deleted/renamed in the file system, RT synchronises them with its
    database.
  - Separating the core and the GUI: Sometimes it happens that big
    changes can't even be noticed. This one won't show you itself, but
    will influence the future a lot.
  - Fixes: Several bugfixes (for example: spot WB, 16 bit TIFF output,
    etc...).

As you may noticed the file browser has improved spectacularly, while
the core+GUI separation needs a lot of time without producing any
visible changes. To release the second milestone all the major and minor
changes has to be polished and tested.  
  
The main goal of final 2.4 stable is batch conversion. All the current
improvements are aiming that final goal, but please remember: batch
conversion comes only with final 2.4 release when every code requirement
is fulfilled.  
  
Meanwhile, for milestone 2 it's impossible to promise any release date,
but it is under heavy  
development and... well, and works, looks and feels better then ever\!

dualon,

RT DevTeam

