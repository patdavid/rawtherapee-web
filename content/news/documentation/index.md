---
title: "Documentation"
date: 2015-03-09T16:21:00+00:00
author: DrSlony
Tags:
- Documentation
- RT 2.4
- RT 3.0
- RT 4.0
---

## RawPedia

RawPedia is the only official source of documentation for users.  
Read RawPedia here:
[rawpedia.rawtherapee.com](http://rawpedia.rawtherapee.com/ "RawPedia")


Developing raw images is a breeze when you know what it is you want to
achieve, and when you know the tools you have at your disposal. The
RawPedia wiki contains articles which describe every tool and topic
related to RawTherapee, and more. Reading it will answer most of your
questions, it will give you greater power and maneuverability in getting
the most out of RawTherapee's tools, and it will save you time\!


Use RawPedia [via Google
Translate](http://translate.google.com/translate?sl=en&tl=fr&u=http%3A%2F%2Frawpedia.rawtherapee.com "RawPedia")
to read the latest information in your language, or help translate
RawPedia into your language - click
[here](https://discuss.pixls.us/t/localization-how-to-translate-rawtherapee-and-rawpedia/2594 "Localization guide")
to find out how.

