---
title: "The RawTherapee Open Source Management Comittee"
date: 2010-10-08T00:06:00+00:00
author: dualon
Tags:
- Community
- OSMC
---



The brief history and the most active contributors were mentioned in our
[last article](http://rawtherapee.com/?mitem=1&artid=50). This time the
future of RawTherapee will be discussed.  
  
Opening the source was an excellent step, it has resulted in a lot of
new features and involved a bunch of really enthusiastic volunteers
already. However, without a pacemaker person or comittee the project
seems to just drift, the need for a
centralized steering "institute" is stronger with every day. The most
successful open source projects all have a "head", be it one person or a
group.  
  
Please meet the Rawtherapee Open Source Management Comittee, or in short
the RT OSMC\!  
  
The goals of OSMC are:  
• Plan the short-, mid- and long-term roadmap of RT,  
• Coordinate the efforts of the active developers and builders,  
• Accept and release milestones,  
• Communicate and mediate the project changes to the public,  
• Work in a transparent way (e.g. devs and the community should see and
understand what is happening).  
  
The current members of the OSMC:  
• Gábor Horváth, aka gabor,  
• Jan Rinze, aka janrinze,  
• Paul Matthijsse, aka paul.matthijsse,  
• Maciek Dworak, aka DrSlony,  
• Wyatt Olson, aka TheBigOne, and  
• David M. Gyurko, aka dualon.  
  
The OSMC is dedicated to steer the community, not to force it in any
way. The developers will be \*asked\* to implement features or fix
distinct bugs, but any dev can reject such an ask or suggest a different
approach, even a new task.

The public will get a line on the events through the website and the
forums. A new e-mail address will be set up too to let anyone contact
the OSMC directly. The forum thread related to the born of OSMC [can be
found here](http://rawtherapee.com/forum/viewtopic.php?t=2215).

The roadmaps will be based on developer and user feedbacks, polls and
current mainstream achievements. They will highlight the tasks to
implement in order of importance and will be publicly discussed.

RawTherapee already has some of the best algorythms and the community
highly appreciates the efforts of the contributors. We beleive that OSMC
will create the stability and the proper background that all the serious
contributors need and deserve.

Everyone is invited to give us feedback, bug reports, testing hours and
new pieces of code - lets make our favorite converter to a wonderful
masterpiece\!  
  
\-- dualon

